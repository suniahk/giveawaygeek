<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/public
 * @author     Alex Watson <cleric.techie@gmail.com>
 */
class Gg_contest_database_Public {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gg_contest_database_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gg_contest_database_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		 // DEVELOPER NOTE: New flags codes found @ https://www.iso.org/obp/ui#search

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gg_contest_database-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( "pickadate", plugin_dir_url( __FILE__ ) . 'css/pickadate/default.css' );
		wp_enqueue_style( "flag-icon", plugin_dir_url( __FILE__ ) . 'css/flag-icon.css' );
		wp_enqueue_style( "pickadate-date", plugin_dir_url( __FILE__ ) . 'css/pickadate/default.date.css' );
		wp_enqueue_style( "fontawesome", plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gg_contest_database_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gg_contest_database_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gg_contest_database-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( "sortable", plugin_dir_url( __FILE__ ) . 'js/sortable.js' );
		wp_enqueue_script( "pickadate", plugin_dir_url( __FILE__ ) . 'js/pickadate/picker.js', array( 'jquery' ) );
		wp_enqueue_script( "pickadate-date", plugin_dir_url( __FILE__ ) . 'js/pickadate/picker.date.js', array( 'jquery' ) );
		wp_enqueue_script( "pickadate-legacy", plugin_dir_url( __FILE__ ) . 'js/pickadate/legacy.js', array( 'jquery' ) );

	}

	private function _custom_nav_menu_item( $title, $url, $order, $parent = 0 ){
		$item = new stdClass();
		$item->ID = 1000000 + $order + $parent;
		$item->db_id = $item->ID;
		$item->title = $title;
		$item->url = $url;
		$item->menu_order = $order;
		$item->menu_item_parent = $parent;
		$item->type = '';
		$item->type_label = '';
		$item->object = '';
		$item->object_id = '';
		$item->classes = array();
		$item->target = '';
		$item->attr_title = '';
		$item->description = '';
		$item->xfn = '';
		$item->status = '';
		return $item;
	  }

	public function registerCategoryMenuItems ( $items, $menu ) {
		global $wpdb;

		$categories = $wpdb->get_results( "SELECT * FROM  " . $wpdb->prefix . "gg_database_category" );
		$count = 0;

		if( $menu->slug=="header" ) {			
			foreach( $items as $menuItem ) {
				if( $menuItem->title == "View All Sweepstakes" ) {
					foreach( $categories as $category ) {
						$items[]=$this->_custom_nav_menu_item( $category->category_name, $menuItem->url . "?category=" . $category->id, 100050 + $count++, $menuItem->db_id );
					}
				}
			}
		}

		return $items;
	}

	public function toggleFavourite() {
		if( isset( $_GET[ "toggle_favorite" ] ) ) {
			if( isset( $_COOKIE[ "favorite" ] ) ) {
				$cookie = json_decode( stripslashes( $_COOKIE[ "favorite" ] ), true );
			} else {
				$cookie = array();
			}

			if( sizeof( $cookie ) > 0 && isset( $cookie[ $_GET[ "toggle_favorite" ] ] ) ) { 
				$cookie[ $_GET[ "toggle_favorite" ] ] = !$cookie[ $_GET[ "toggle_favorite" ] ];
			} else {
				$cookie[ $_GET[ "toggle_favorite" ] ] = true;
			} 

			setcookie( "favorite", json_encode ($cookie ), time() + +86400*90 );

			$url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

			$newUrl=str_replace( "?toggle_favorite=" . $_GET[ "toggle_favorite" ], "", $url );
			$newUrl=str_replace( "&toggle_favorite=" . $_GET[ "toggle_favorite" ], "", $newUrl );

			header( "Location:" . $newUrl );
			exit;
		}
	}

	public function saveContest() {
		global $wpdb;

		$countries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_country" );
		$categories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_category" );
		$errorCode = 0;

		if( isset( $_POST[ 'g-recaptcha-response' ] ) ) {
			$url = sanitize_text_field( $_POST[ 'formUrl' ] );

			$ch = curl_init();

			curl_setopt_array($ch, [
				CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => [
					'secret' => "6LdHuDYUAAAAAPjh9zhG1bjCK2V0FhZJMyU-9bwv",
					'response' => $_POST[ 'g-recaptcha-response' ],
					'remoteip' => $_SERVER['REMOTE_ADDR']
				],
				CURLOPT_RETURNTRANSFER => true
			]);

			$output = curl_exec($ch);
			curl_close($ch);
	
			$recaptchaResult = json_decode( $output );
			
			if( $recaptchaResult->success ) {

				if( isset( $_POST[ "contestName" ] ) && $_POST[ "contestName" ] != "" &&
					isset( $_POST[ "contestUrl" ] ) && $_POST[ "contestUrl" ] != "" &&
					isset( $_POST[ "contestDescription" ] ) && $_POST[ "contestDescription" ] != "" &&
					isset( $_POST[ "endDate" ] ) && $_POST[ "endDate" ] != "" &&
					isset( $_POST[ "category" ] ) && $_POST[ "category" ] != "" ) 
				{
					$contestUrl = sanitize_text_field( $_POST[ "contestUrl" ] );
					if( !strpos( $contestUrl, "//" ) || strpos( $contestUrl, "//" ) > 10 ) {
						$contestUrl = "//" . $contestUrl;
					}

					$newContest = array(
						"name"=>sanitize_text_field( $_POST[ "contestName" ] ),
						"url"=> $contestUrl,
						"description"=>sanitize_text_field( $_POST[ "contestDescription" ] ),
						"minimum_age"=>sanitize_text_field( $_POST[ "ageRestriction" ] ),
						"end_date"=>sanitize_text_field( $_POST[ "endDate" ] ),
					);

					if( $wpdb->insert( $wpdb->prefix . "gg_database_contest", $newContest ) ) {
						$errors = false;
						$contest_id = $wpdb->insert_id;
						foreach( $countries as $country ) {
							if( isset( $_POST[ "contestCountry" . $country->country_code ] ) ) {
								$addCountry = array( 
									"country_id"=> sanitize_text_field( $_POST[ "contestCountry" . $country->country_code ] ),
									"contest_id" => $contest_id
								);
								if( !$wpdb->insert( $wpdb->prefix . "gg_contest_country", $addCountry ) ) {
									$errors = true;
									$errorCode += 64;
								}
							}
						}
						// Dev note: support for multiple categories should the need come up
						if( isset( $_POST[ "category" ] ) ) {
							$addCategory = array( 
								"category_id"=> sanitize_text_field( $_POST[ "category" ] ),
								"contest_id" => $contest_id
							);
							if( !$wpdb->insert( $wpdb->prefix . "gg_contest_category", $addCategory ) ) {
								$errors = true;
								$errorCode += 128;
							}
						}
						if( !$errors ) {
							if( !isset( $_GET[ 'savedContest' ] ) ) {
								if( strpos( $url, "?" ) > 0 ) {
									$url = $url . "&";
								} else {
									$url= $url . "?";
								}

								$url = $url . "savedContest=true";
							}
						}
					} else {
						$errorCode += 32;
					}
				} else {
					if( !isset( $_POST[ "contestName" ] ) || $_POST[ "contestName" ] == "" ) {
						$errorCode += 1;
					}

					if( !isset( $_POST[ "contestUrl" ] ) || $_POST[ "contestUrl" ] == "" ) {
						$errorCode += 2;
					}

					if( !isset( $_POST[ "contestDescription" ] ) || $_POST[ "contestDescription" ] == "" ) {
						$errorCode += 4;
					}
					
					if( !isset( $_POST[ "endDate" ] ) || $_POST[ "endDate" ] == "" ) {
						$errorCode += 8;
					}

					if( !isset( $_POST[ "category" ] ) || $_POST[ "category" ] == "" ) {
						$errorCode += 16;
					}
				}
			} else {
				$errorCode += 256;
			}

			if( strpos( $url, "?" ) > 0 ) {
				parse_str( substr( $url, strpos( $url, "?" ) + 1 ), $queryValues );

				$queryValues[ "errorCode" ] = $errorCode;

				$url = substr( $url, 0, strpos( $url, "?" ) - 1 ) . "?" . http_build_query( $queryValues );
			} else {
				$url = $url . "?errorCode=" . $errorCode;
			}
			wp_redirect( $url );
		} else {
			wp_redirect( home_url() . "?recaptchaResponse=0" );
		}
    } 

	public function display_list_shortcode( $atts ) {
		$atts = shortcode_atts( array( "featuredcount" => 2, "contestsperpage" => 10 ), array_change_key_case( (array)$atts ) );

		require_once( plugin_dir_path( __FILE__ ) . 'partials/gg_contest_database-public-display-list.php' );
	}

	public function display_form_shortcode() {
		require_once( plugin_dir_path( __FILE__ ) . 'partials/gg_contest_database-public-display-form.php' );
	}

	public function display_latest_shortcode( $atts ) {
		$atts = shortcode_atts( array( "days" => 7 ), array_change_key_case( (array)$atts ) );

		require_once( plugin_dir_path( __FILE__ ) . 'partials/gg_contest_database-public-display-latest.php' );
	}

}
