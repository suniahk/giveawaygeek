<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/public/partials
 */

 global $wpdb;

 $cookie = array();
 
 if( isset( $_COOKIE[ "favorite" ] ) ) {
    $cookie = json_decode( stripslashes( $_COOKIE[ "favorite" ] ), true );
 }

 $pageNumber = isset( $_GET["contestPage"] )?intval($_GET["contestPage"])-1:0;
 $currentCategory = isset( $_GET[ "category" ] )?intval( $_GET[ "category" ] ):-1;
$orderBy="end_date DESC";
$sortMetrics = array(
    "submit_date",
    "name",
    "end_date",
    "age_minimum"
);

$sortValue = -1;
$sortDirectionValue = 1;

 if( isset( $_POST[ "sortBy" ] ) ) {
    $sortValue = intval( $_POST[ "sortBy" ] );
    $orderBy = $sortMetrics[ $sortValue ];
    if( intval( $_POST[ "sortByDirection" ] ) == 2 ) {
        $sortDirectionValue = 2;
        $orderBy = $orderBy . " DESC";
    }
 }
 $idArray = "";

 $databaseResults = null;

 if( $currentCategory != -1 || ( isset( $_GET[ "showFavorites" ] ) && intval( $_GET[ "showFavorites" ] ) == 1 ) ) {
    $idArray = "";
    if( $currentCategory != -1 ) {
        $validIds = $wpdb->get_results( "SELECT DISTINCT contest_id FROM " . $wpdb->prefix . "gg_contest_category WHERE category_id = " . $currentCategory );

        $validIds = array_map( function( $idObject ) {
            return $idObject->contest_id;
        }, $validIds );

        if( isset( $_GET[ "showFavorites" ] ) && intval( $_GET[ "showFavorites" ] ) == 1 ) {
            $validIds = array_intersect( array_keys( $cookie ), $validIds );
        }

        $idArray = implode( ",", $validIds );
    } elseif( isset( $_GET[ "showFavorites" ] ) && intval( $_GET[ "showFavorites" ] ) == 1 ) {
        $idArray = implode( ",", array_keys( $cookie ) );
    }

    if( strlen( $idArray ) > 0 ) {
        $databaseResults = $wpdb->get_results( "SELECT *  FROM " . $wpdb->prefix . "gg_database_contest contests WHERE DATE(end_date)>=CURDATE() AND is_featured=false AND id IN( " . $idArray . " ) ORDER BY " . $orderBy . " LIMIT " . ( intval( $atts[ "contestsperpage" ] ) - intval( $atts[ "featuredcount" ] ) ) * $pageNumber . ", " . ( intval( $atts[ "contestsperpage" ] ) - intval( $atts[ "featuredcount" ] ) ) );
    }
} else {
    $databaseResults = $wpdb->get_results( "SELECT *  FROM " . $wpdb->prefix . "gg_database_contest contests WHERE DATE(end_date)>=CURDATE() AND is_featured=false ORDER BY " . $orderBy . " LIMIT " . ( intval( $atts[ "contestsperpage" ] ) - intval( $atts[ "featuredcount" ] ) ) * $pageNumber . ", " . ( intval( $atts[ "contestsperpage" ] ) - intval( $atts[ "featuredcount" ] ) ) );

}

$featuredResults = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_contest WHERE DATE(end_date)>=CURDATE() AND is_featured=true ORDER BY -LOG(1-RAND())/weight LIMIT " . $atts[ "featuredcount" ] );
$contestCount = 0;

if( $databaseResults != null ) {
    if( $featuredResults != null ) {
        $contestCount += count( $featuredResults );
    }
    $contestCount += count( $databaseResults );
    if( $contestCount != 0 ) {
        $numberOfPages = ceil($contestCount / ( intval( $atts[ "contestsperpage" ] ) - intval( $atts[ "featuredcount" ] ) ) );
    } else {
        $numberOfPages = 0;
    } ?>
    <form method="POST" id="sortForm">
        Sort By 
        <select id="sortBy" name="sortBy">
            <option value="0" <?php if( $sortValue == 0 ) { ?>selected="selected"<?php } ?>>Post Date</option>
            <option value="1" <?php if( $sortValue == 1 ) { ?>selected="selected"<?php } ?>>Name</option>
            <option value="2" <?php if( $sortValue == 2 ) { ?>selected="selected"<?php } ?>>End Date</option>
            <option value="3" <?php if( $sortValue == 3 ) { ?>selected="selected"<?php } ?>>Age Minimum</option>
        </select>
        <select id="sortByDirection" name="sortByDirection">
            <option value="1" <?php if( $sortDirectionValue == 1 ) { ?>selected="selected"<?php } ?>>Ascending</option>
            <option value="2" <?php if( $sortDirectionValue == 2 ) { ?>selected="selected"<?php } ?>>Decending</option>
        </select>
        <input type="submit" value="Sort" />
    </form>
    <span class="favoritesButton">
        <label for="showFavorites"><input type="checkbox" value="showFavorites" <?php if( isset( $_GET[ "showFavorites" ] ) && intval( $_GET[ "showFavorites" ] ) == 1 ) { echo "checked='checked'"; } ?> class="showFavoritesButton" id="showFavorites" name="showFavorites" /> Load Favorites Only (Refreshes on Click)</label>
    </span>
    <div style="clear : both"></div>
        <ul class="contests">
        <?php
        if( $featuredResults != null ) {
         foreach( $featuredResults as $contest ) { 
            $contestCountries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_country reference LEFT JOIN " . $wpdb->prefix . "gg_database_country country ON ( reference.country_id=country.id ) WHERE contest_id=" . $contest->id );
            $contestCategories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_category reference LEFT JOIN " . $wpdb->prefix . "gg_database_category category ON ( reference.category_id=category.id ) WHERE contest_id=" . $contest->id );
        ?>
            <li data-contest-link="<?php echo $contest->url; ?>">
                <h3><span class="featuredBadge">Featured</span><?php 
                echo stripslashes( $contest->name ); 
                ?>
                <span class="contestLink">
                    <a href="<?php echo $contest->url ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                </span>
                </h3>
                <span class="endDate">Contest ends <?php echo date( "M j, Y", strtotime( $contest->end_date ) ); ?></span>
                <span class="minimumAge"> Minimum Age: <?php echo ($contest->minimum_age!=0)?$contest->minimum_age:"None"; ?></span>
                <div><?php echo stripslashes( $contest->description ); ?></div>
                
                <?php 
                $categoryCount = count($contestCategories);

                if( $categoryCount > 0 ) {
                    ?> <div class="categories">Posted In  <?php 
                
                    foreach( $contestCategories as $category ) { 
                        echo $category->category_name;
                        $last_iteration = !(--$categoryCount);
                        if( !$last_iteration ) {
                            echo ", ";
                        }
                    }
                    ?>
                    <span class="flags">
                    <?php
                    foreach( $contestCountries as $country ) { 
                        ?> <span class="flag-icon flag-icon-<?php echo strtolower( $country->country_code ); ?>"></span> <?php
                    }
                    ?>
                    
                    <a href="?toggle_favorite=<?php echo $contest->id; ?>" class="propagateBlock">
                    <?php
                    if( isset( $cookie[ $contest->id ] ) && $cookie[ $contest->id ] ) { ?>
                        <img class="favouriteImage" src="<?php echo plugin_dir_url( __FILE__ ) . ".."; ?>/images/star_full.png" alt="Star" />
                    <?php } else { ?>
                        <img class="favouriteImage" src="<?php echo plugin_dir_url( __FILE__ ) . ".."; ?>/images/star_empty.png" alt="Star" />
                    <?php } ?>
                    </a>
                    </span></div> <?php
                }
                ?></li>
        <?php } 
        }
        foreach( $databaseResults as $contest ) { 
            $contestCountries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_country reference LEFT JOIN " . $wpdb->prefix . "gg_database_country country ON ( reference.country_id=country.id ) WHERE contest_id=" . $contest->id );
            $contestCategories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_category reference LEFT JOIN " . $wpdb->prefix . "gg_database_category category ON ( reference.category_id=category.id ) WHERE contest_id=" . $contest->id );
        ?>
            <li data-contest-link="<?php echo $contest->url; ?>">
                <h3><?php 
                echo stripslashes( $contest->name ); 
                ?>
                <span class="contestLink">
                    <a href="<?php echo $contest->url ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                </span>
                </h3>
                <span class="endDate">Contest ends <?php echo date( "M j, Y", strtotime( $contest->end_date ) ); ?></span>
                <span class="minimumAge"> Minimum Age: <?php echo ($contest->minimum_age!=0)?$contest->minimum_age:"None"; ?></span>
                <div><?php echo stripslashes( $contest->description ); ?></div>
                <?php 
                $categoryCount = count($contestCategories);

                if( $categoryCount > 0 ) {
                    ?> <div class="categories">Posted In  <?php 
                
                    foreach( $contestCategories as $category ) { 
                        echo $category->category_name;
                        $last_iteration = !(--$categoryCount);
                        if( !$last_iteration ) {
                            echo ", ";
                        }
                    }
                    ?>
                    <span class="flags">
                <?php
                foreach( $contestCountries as $country ) { 
                    ?> <span class="flag-icon flag-icon-<?php echo strtolower( $country->country_code ); ?>"></span> <?php
                }
                ?>
                <a href="?toggle_favorite=<?php echo $contest->id; ?>" class="propagateBlock">
                    <?php
                    if( isset( $cookie[ $contest->id ] ) && $cookie[ $contest->id ] ) { ?>
                        <img class="favouriteImage" src="<?php echo plugin_dir_url( __FILE__ ) . ".."; ?>/images/star_full.png" alt="Star" />
                    <?php } else { ?>
                        <img class="favouriteImage" src="<?php echo plugin_dir_url( __FILE__ ) . ".."; ?>/images/star_empty.png" alt="Star" />
                    <?php } ?>
                </a>
                </span><div>
                     <?php
                } ?>
            </li>
        <?php } ?>
    </ul>
    <?php
    if( $numberOfPages != 0 ) {
    ?>
    <div class="pagination">
        <a href="#">First</a>
        &nbsp;<a href="?contestPage=<?php echo $pageNumber; ?>">«</a>
        
        <?php if( $pageNumber - 1 > 0  ) { ?>
            <a href="?contestPage=<?php echo $pageNumber - 1; ?>"><?php echo $pageNumber - 1; ?></a>
        <?php }
        if( $pageNumber != 0 ) { ?>
            <a href="?contestPage=<?php echo $pageNumber; ?>"><?php echo $pageNumber; ?></a>
        <?php } ?>
        <strong><?php echo $pageNumber + 1; ?></strong>
        <?php if( $pageNumber + 2 <= $numberOfPages  ) { ?>
            <a href="?contestPage=<?php echo $pageNumber + 2; ?>"><?php echo $pageNumber + 2; ?></a>
        <?php }
        if( $pageNumber + 3 <= $numberOfPages ) { ?>
            <a href="?contestPage=<?php echo $pageNumber + 3; ?>"><?php echo $pageNumber + 3; ?></a>
        <?php } ?>
        
        <a href="?contestPage=<?php echo $pageNumber + 2; ?>">»</a>
        &nbsp;<a href="#">Last</a>
    </div>
    <?php } ?>
    <script type="text/javascript">
    jQuery( function() {
        jQuery( ".propagateBlock" ).click( function( event ) {
            event.stopPropagation();
        } );
        jQuery(".contests li").click( function( event ) {
            event.preventDefault();
            window.location.href = jQuery( event.currentTarget ).data( "contest-link" );
        });
        jQuery( ".showFavoritesButton" ).change( function( event ) {
            var url = window.location.href;

            if( jQuery( event.currentTarget ).is( ":checked" ) ) {
                if( url.indexOf( "showFavorites" ) > 0 ) {
                    url.replace( "?showFavorites=0", "?showFavorites=1" );
                    url.replace( "&showFavorites=0", "&showFavorites=1" );
                } else {
                    if( url.indexOf( "?" ) > 0 ) {
                        url += "&showFavorites=1";
                    } else {
                        url += "?showFavorites=1";
                    }
                }
            } else {
                if( url.indexOf( "showFavorites" ) > 0 ) {
                    url = url.replace( "?showFavorites=1", "" );
                    url = url.replace( "&showFavorites=1", "" );
                    url = url.replace( "?showFavorites=0", "" );
                    url = url.replace( "&showFavorites=0", "" );
                }
            }
            window.location.href = url;
        } );
    } );
    </script>
<?php
} else {
    echo "No results found.";
}
?>