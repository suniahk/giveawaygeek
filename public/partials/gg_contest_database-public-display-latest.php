<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/public/partials
 */

 global $wpdb;

 $cookie = array();

 if( isset( $_COOKIE[ "favorite" ] ) ) {
    $cookie = json_decode( stripslashes( $_COOKIE[ "favorite" ] ), true );
 }

$databaseResults = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_contest WHERE DATE(submit_date) > DATE_SUB(CURDATE(), INTERVAL " . $atts[ "days" ] . " DAY) AND DATE(end_date)>=CURDATE()" );

if( $databaseResults != null ) {?>
    <ul class="contests">
        <?php
        foreach( $databaseResults as $contest ) { 
            $contestCountries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_country reference LEFT JOIN " . $wpdb->prefix . "gg_database_country country ON ( reference.country_id=country.id ) WHERE contest_id=" . $contest->id );
            $contestCategories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_category reference LEFT JOIN " . $wpdb->prefix . "gg_database_category category ON ( reference.category_id=category.id ) WHERE contest_id=" . $contest->id );
        ?>
            <li data-contest-link="<?php echo $contest->url; ?>">
                <h3><?php 
                echo stripslashes( $contest->name ); 
                ?>
                <span class="contestLink">
                    <a href="<?php echo $contest->url ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
                </span>
                </h3>
                <span class="endDate">Contest ends <?php echo date( "M j, Y", strtotime( $contest->end_date ) ); ?></span>
                <span class="minimumAge"> Minimum Age: <?php echo ($contest->minimum_age!=0)?$contest->minimum_age:"None"; ?></span>
                <div><?php echo stripslashes( $contest->description ); ?></div>
                <?php 
                $categoryCount = count($contestCategories);

                if( $categoryCount > 0 ) {
                    ?> <div class="categories">Posted In  <?php 
                
                    foreach( $contestCategories as $category ) { 
                        echo $category->category_name;
                        $last_iteration = !(--$categoryCount);
                        if( !$last_iteration ) {
                            echo ", ";
                        }
                    }
                    ?> 
                    <span class="flags">
                    <?php
                    foreach( $contestCountries as $country ) { 
                        ?> <span class="flag-icon flag-icon-<?php echo strtolower( $country->country_code ); ?>"></span> <?php
                    }
                    ?>
                    
                    <a href="?toggle_favorite=<?php echo $contest->id; ?>" class="propagateBlock">
                    <?php
                    if( isset( $cookie[ $contest->id ] ) && $cookie[ $contest->id ] ) { ?>
                        <img class="favouriteImage" src="<?php echo plugin_dir_url( __FILE__ ) . ".."; ?>/images/star_full.png" alt="Star" />
                    <?php } else { ?>
                        <img class="favouriteImage" src="<?php echo plugin_dir_url( __FILE__ ) . ".."; ?>/images/star_empty.png" alt="Star" />
                    <?php } ?>
                    </a>
                    </span></div> <?php
                } ?>
            </li>
        <?php } ?>
    </ul>
<?php
} else {
    echo "No contests have been submitted in the last " . $atts[ "days" ] . " days.";
}
?>