<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/public/partials
 */

 global $wpdb;
 
$countries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_country" );
$categories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_category" );

if( isset( $_GET[ "savedContest" ] ) ) { ?>
    <div class="positive notificationBox">
        Your contest was saved successfully!
    </div>
<?php } else if( isset( $_GET[ "errorCode" ] ) ) {
    $errorList = array( 
        "Your contest needs a name.",
        "Your contest needs to have a URL.",
        "Your contest needs a description.",
        "When does your contest end?",
        "What category is your contest in?",
        "There was a problem trying to submit your contest.  Please try again or contact The Giveaway Geek.",
        "There was a problem saving the country that your contest is valid for. Please try again or contact The Giveaway Geek.",
        "There was a problem saving the category that your contest is in. Please try again or contact The Giveaway Geek.",
        "Your reCaptcha response was invalid.  Please try again."
    );

     ?>
     <div class="negative notificationBox">
        There was a problem with your contest.
        <ul>
            <?php 
            $errorValues = strrev( strval( decbin( $_GET[ "errorCode" ] ) ) );
            for( $error = 0; $error < strlen( $errorValues ); $error++ ) {
                if( $errorValues[ $error ] == "1" ) {
                ?>
                <li><?php echo $errorList[ $error ]; ?></li>
            <?php }
        } ?>
        </ul>
     </div>
<?php } ?>

<script type="text/javascript">
    function formSubmit( token ) {
        document.getElementById( "contestForm" ).submit();
    }
</script>

<form id="contestForm" method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" class="gg-contest-form">
    <input type="hidden" name="action" value="contest_form">
    <div class="half-width">
        <label for="contestName">Contest Name*</label>
        <input type="text" name="contestName" id="contestName" placeholder="My Amazing Contest" />
    </div>
    <div class="half-width">
        <label for="contestUrl">Contest Url*</label>
        <input type="text" name="contestUrl" id="contestUrl" placeholder="http://website.com/contest" />
    </div>
    <div>
        <label for="contestDescription">Description*</label>
        <textarea name="contestDescription" id="contestDescription"></textarea>
    </div>
    <div class="half-width">
        <label>Countries Available*</label><br />
        <?php
            foreach( $countries as $country ) {
        ?>
        <label for="contestCountry<?php echo $country->country_code; ?>">
        <input type="checkbox" name="contestCountry<?php echo $country->country_code; ?>" id="contestCountry<?php echo $country->country_code; ?>" value="<?php echo $country->id; ?>" />
        <span class="flag-icon flag-icon-<?php echo strtolower( $country->country_code ); ?>"></span>
        <?php echo $country->country_name; ?>
        </label><br />
        <?php } ?>
    </div>
    <div class="half-width">
        <label for="category">Category*</label><br />
        <select name="category" id="category">
            <?php foreach( $categories as $category ) { ?>
                <option value="<?php echo $category->id; ?>"><?php echo $category->category_name; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="half-width">
        <label for="ageRestriction">Age Restriction</label>
        <input type="text" name="ageRestriction" id="ageRestriction" />
    </div>
    <div class="half-width">
        <label for="endDate">End Date*</label>
        <input type="text" name="endDate" id="endDate" />
    </div>
    <div>
        (*) Required
    </div>
    <input type="hidden" name="formUrl" value="<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>" />
    <button value="Submit Contest" name="submitContest" id="submitContest" class="g-recaptcha" data-sitekey="6LdHuDYUAAAAAFnLaPTxWqQcw8bJ3Jcpk1IDJiW7" data-callback="formSubmit">Submit Contest</button>
</form>
<script type="text/javascript">
jQuery( document ).ready( function() {
    jQuery('#endDate').pickadate({ format: 'yyyy-mm-dd' });
} );
</script>

<script type="text/javascript" src="https://www.google.com/recaptcha/api.js" async defer></script>