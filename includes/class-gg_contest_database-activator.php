<?php

/**
 * Fired during plugin activation
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/includes
 * @author     Alex Watson <cleric.techie@gmail.com>
 */

class Gg_contest_database_Activator {
	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */

	public static function activate() {
		global $wpdb;

		$sql[0]="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "gg_database_country (
			id MEDIUMINT( 9 ) NOT NULL AUTO_INCREMENT,
			country_name TEXT NOT NULL,
			country_code VARCHAR( 4 ) NOT NULL,
			PRIMARY KEY ( id )
		) " . $wpdb->get_charset_collate() . ";";
		
		$sql[1]="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "gg_database_category (
			id MEDIUMINT( 9 ) NOT NULL AUTO_INCREMENT,
			category_name TEXT NOT NULL,
			PRIMARY KEY ( id )
		) " . $wpdb->get_charset_collate() . ";";

		$sql[2] = "CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "gg_database_contest (
			id MEDIUMINT( 9 ) NOT NULL AUTO_INCREMENT,
  			submit_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  			end_date DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
			url TEXT NOT NULL,
			description TEXT NOT NULL,
			name TEXT NOT NULL,
			minimum_age SMALLINT( 3 ) DEFAULT 0,
			is_featured BOOLEAN DEFAULT false NOT NULL,
			weight SMALLINT DEFAULT 1,
			PRIMARY KEY ( id )
		) " . $wpdb->get_charset_collate() . ";";

		$sql[3]="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix . "gg_contest_category (
			contest_id MEDIUMINT( 9 ) NOT NULL,
			category_id MEDIUMINT( 9 ) NOT NULL,
			FOREIGN KEY( contest_id ) REFERENCES " . $wpdb->prefix . "gg_database_contest( id ) ON DELETE CASCADE,
			FOREIGN KEY( category_id ) REFERENCES " . $wpdb->prefix . "gg_database_category( id ) ON DELETE CASCADE
		) " . $wpdb->get_charset_collate() . ";";

		$sql[4]="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix . "gg_contest_country (
			contest_id MEDIUMINT( 9 ) NOT NULL,
			country_id MEDIUMINT( 9 ) NOT NULL,
			FOREIGN KEY( contest_id ) REFERENCES " . $wpdb->prefix . "gg_database_contest( id ) ON DELETE CASCADE,
			FOREIGN KEY( country_id ) REFERENCES " . $wpdb->prefix . "gg_database_country( id ) ON DELETE CASCADE
		) " . $wpdb->get_charset_collate() . ";";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		/* ONLY UNCOMMENT IF THE DATABASE GETS OUT OF SYNC
		dbDelta( "DROP TABLE IF EXISTS " . $wpdb->prefix . "gg_database_country;" );
		dbDelta( "DROP TABLE IF EXISTS " . $wpdb->prefix . "gg_database_category;" );
		dbDelta( "DROP TABLE IF EXISTS " . $wpdb->prefix . "gg_database_contest;" );
		dbDelta( "DROP TABLE IF EXISTS " . $wpdb->prefix . "gg_contest_category;" );
		dbDelta( "DROP TABLE IF EXISTS " . $wpdb->prefix . "gg_contest_country;" );
		*/

		foreach( $sql as $query ) {
			dbDelta( $query );
		}

		$countries=array( 
			array( "country_name" => "United States", "country_code" => "US" ),
			array( "country_name" => "Canada", "country_code" => "CA" ),
			array( "country_name" => "International", "country_code" => "INTL" )
		 );

		foreach( $countries as $country ) {
			if( $wpdb->get_results( "SELECT * from " . $wpdb->prefix . "gg_database_country WHERE country_name = '" . $country["country_name"] . "'" ) == null ) {
				$wpdb->insert( $wpdb->prefix . "gg_database_country", $country );
			}
		}
	}

}
