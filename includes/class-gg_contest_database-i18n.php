<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/includes
 * @author     Alex Watson <cleric.techie@gmail.com>
 */
class Gg_contest_database_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gg_contest_database',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
