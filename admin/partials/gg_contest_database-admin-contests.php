<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/admin/partials
 */

 global $wpdb;
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php 
$featuredResults = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_contest WHERE DATE(end_date)>=CURDATE() AND is_featured=true ORDER BY weight DESC, end_date DESC" );
$databaseResults = $wpdb->get_results( "SELECT *  FROM " . $wpdb->prefix . "gg_database_contest contests WHERE DATE(end_date)>=CURDATE() AND is_featured=false ORDER BY submit_date DESC" );
$expiredContests = $wpdb->get_results( "SELECT COUNT(id) as count FROM " . $wpdb->prefix . "gg_database_contest WHERE DATE(end_date)<=CURDATE()" );

if( isset( $_GET[ "deletedContest" ] ) && $_GET[ "deletedContest" ] === "true" ) { ?>
    <div class="feedback confirm">
        Your contest was deleted successfully!
    </div>
<?php } elseif( isset( $_GET[ "deletedContest" ] ) ) {  ?>
    <div class="feedback negative">
        Your contest was not deleted successfully.
    </div>
<?php } ?>

<h1>GiveawayGeek Contest Admin</h1>
<?php
    if( $featuredResults != null ) { ?>
        <h2>Featured Contests</h2>
        <table class="sortable adminContestTable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Ends</th>
                    <th>Age Minimum</th>
                    <th>Category</th>
                    <th>Created On</th>
                    <th>Weight</th>
                    <th rowspan="2">Actions</th> 
                </tr>
                <tr>
                    <th class="sorttable_nosort" colspan="4">Description</th>
                    <th class="sorttable_nosort" colspan="2">Link</th>
                </tr>
            </thead>
            <?php foreach( $featuredResults as $contest ) { 
            $contestCountries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_country reference LEFT JOIN " . $wpdb->prefix . "gg_database_country country ON ( reference.country_id=country.id ) WHERE contest_id=" . $contest->id );
            $contestCategories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_category reference LEFT JOIN " . $wpdb->prefix . "gg_database_category category ON ( reference.category_id=category.id ) WHERE contest_id=" . $contest->id );
        ?>
            <tr class="contestRow">
                <td>
                    <a href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?id=<?php echo $contest->id; ?>&action=remove_featured_status&url=<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <span class="featuredBadge">Featured</span>
                    </a>
                    <?php echo $contest->name;
                    foreach( $contestCountries as $country ) { ?> 
                        <span class="flag-icon flag-icon-<?php echo strtolower( $country->country_code ); ?>"></span>
                    <?php } ?>
                </td>
                <td class="centered"><?php echo date( "M j, Y", strtotime( $contest->end_date ) ); ?></td>
                <td class="centered"><?php echo ($contest->minimum_age!=0)?$contest->minimum_age:"No"; ?></td>
                <td><?php 
                    $i = count($contestCategories);
                    foreach( $contestCategories as $category ) { 
                        echo $category->category_name;
                        $last_iteration = !(--$i);
                        if( !$last_iteration ) {
                            echo ", <br />";
                        }
                    }?>
                </td>
                <td class="centered"><?php echo date( "M j, Y", strtotime( $contest->submit_date ) ); ?></td>
                <td class="centered">
                    <a href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?id=<?php echo $contest->id; ?>&action=set_weight&weight=<?php echo $contest->weight+1; ?>&url=<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <span class="icon icon-plus"></span>
                    </a>
                    <?php echo $contest->weight; ?> 
                    <?php if( $contest->weight > 1 ) { ?> 
                        <a href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?id=<?php echo $contest->id; ?>&action=set_weight&weight=<?php echo $contest->weight-1; ?>&url=<?php echo $_SERVER['REQUEST_URI']; ?>">
                            <span class="icon icon-minus"></span>
                        </a>
                    <?php } ?>    
                    </td>
                <td rowspan="2" class="centered"><a data-category="<?php echo $contest->id; ?>" class="contestDeleteButton" href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?action=contest_delete&id=<?php echo $contest->id ?>&url=<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
            </tr>
            <tr class="contestRow">
                <td colspan="4"><?php echo $contest->description; ?></td>
                <td colspan="2"><?php echo $contest->url; ?></td>
            </tr>
            <?php } ?>
        </table>
    <?php } 
    if( $databaseResults != null ) {?>
    <h2>Normal Contests</h2>
    <table class="sortable adminContestTable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Ends</th>
                <th>Age Minimum</th>
                <th>Category</th>
                <th>Created On</th>
                <th rowspan="2">Actions</th>
            </tr>
            <tr>
                <th class="sorttable_nosort" colspan="3">Description</th>
                <th class="sorttable_nosort" colspan="2">Link</th>
            </tr>
        </thead>
        <?php foreach( $databaseResults as $contest ) { 
            $contestCountries = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_country reference LEFT JOIN " . $wpdb->prefix . "gg_database_country country ON ( reference.country_id=country.id ) WHERE contest_id=" . $contest->id );
            $contestCategories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_contest_category reference LEFT JOIN " . $wpdb->prefix . "gg_database_category category ON ( reference.category_id=category.id ) WHERE contest_id=" . $contest->id );
        ?>
            <tr class="contestRow">
                <td><a href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?id=<?php echo $contest->id; ?>&action=make_featured&url=<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <span class="fakeFeaturedBadge">Featured</span>
                </a>
                <?php echo $contest->name;
                foreach( $contestCountries as $country ) { 
                    ?> <span class="flag-icon flag-icon-<?php echo strtolower( $country->country_code ); ?>"></span> <?php
                }?></td>
                <td class="centered"><?php echo date( "M j, Y", strtotime( $contest->end_date ) ); ?></td>
                <td class="centered"><?php echo ($contest->minimum_age!=0)?$contest->minimum_age:"No"; ?></td>
                <td><?php 
                $i = count($contestCategories);
                foreach( $contestCategories as $category ) { 
                    echo $category->category_name;
                    $last_iteration = !(--$i);
                    if( !$last_iteration ) {
                        echo ", <br />";
                    }
                }?></td>
                <td class="centered"><?php echo date( "M j, Y", strtotime( $contest->submit_date ) ); ?></td>
                <td rowspan="2" class="centered"><a data-contest="<?php echo $contest->id; ?>" class="contestDeleteButton" href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?action=contest_delete&id=<?php echo $contest->id ?>&url=<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
            </tr>
            <tr class="contestRow">
                <td colspan="3"><?php echo $contest->description; ?></td>
                <td colspan="2"><?php echo $contest->url; ?></td>
            </tr>
        <?php } ?>
    </table>

    Current Contest Count: <?php echo sizeof( $featuredResults ) + sizeof( $databaseResults ) ?>
    <br />
    Expired Contest Count: <?php echo $expiredContests[0]->count; ?>
    <script type="text/javascript">
    jQuery( ".contestDeleteButton" ).click( function( event ) {
        event.preventDefault();
        if( confirm( "Warning!  This will permanently delete this contest.  Are you sure you want to do this?" ) ) {
            window.location="<?php echo esc_url( admin_url('admin-post.php') ); ?>?action=contest_delete&id=" + jQuery( event.currentTarget ).data( "contest" ) + "&url=<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>";
        }
    } );
</script>
<?php
} else {
    echo "No results found.";
}
?>