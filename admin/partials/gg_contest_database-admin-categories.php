<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/admin/partials
 */

 global $wpdb;
 $categories = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "gg_database_category" );
 $contestCounts = $wpdb->get_results( "SELECT COUNT( contest_id ) as contests, category_id FROM " . $wpdb->prefix . "gg_contest_category GROUP BY category_id" );
 $currentContestCounts = $wpdb->get_results( "SELECT COUNT( contests.id ) as contests, link_id.category_id FROM " . $wpdb->prefix . "gg_database_contest contests LEFT JOIN " . $wpdb->prefix . "gg_contest_category link_id ON contests.id = link_id.contest_id WHERE DATE(contests.end_date)>=CURDATE() GROUP BY link_id.category_id" );

 function getContestCount( $categoryId, $contestCounts ) {
    if( $contestCounts != null ) {
        foreach( $contestCounts as $count ) {
            if( $count->category_id == $categoryId ) {
                return $count->contests;
            }
        }
    }

    return 0;
 }
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php 
if( isset( $_GET[ "savedCategory" ] ) && $_GET[ "savedCategory" ] === "true" ) { ?>
    <div class="feedback confirm">
        Your category was saved successfully!
    </div>
<?php } elseif( isset( $_GET[ "deletedCategory" ] ) && $_GET[ "deletedCategory" ] === "true" ) {  ?>
    <div class="feedback confirm">
        Your category was deleted successfully!
    </div>
<?php } elseif( isset( $_GET[ "savedCategory" ] ) || isset( $_GET[ "deletedCategory" ] ) ) { ?>
    <div class="feedback negative">
        There was a problem with your request.
    </div>
<?php } ?>
<h2>Categories</h2>
<form method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
    <input type="hidden" name="action" value="category_form">
    <input type="text" name="categoryName" />
    <input type="hidden" name="formUrl" value="<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>" />
    <input type="submit" name="submitCategory" value="Add new category" />
</form>

<?php if( $categories != null ) { ?>
<table class="categoryAdminTable">
    <tr>
        <th class="column categoryName">Category Name</th>
        <th class="column categoryContestCount">Number of Contests</th>
        <th class="column categoryActions">Actions</th>
    </tr>
    <?php
    foreach( $categories as $category ) { ?>
        <tr>
            <td class="column categoryName"><?php echo $category->category_name; ?></td>
            <td class="column categoryContestCount"><?php echo getContestCount( $category->id, $currentContestCounts ); ?> (<?php echo getContestCount( $category->id, $contestCounts ); ?> Total)</td>
            <td class="column categoryActions"><a data-category="<?php echo $category->id; ?>" class="categoryDeleteButton" href="<?php echo esc_url( admin_url('admin-post.php') ); ?>?action=category_delete&id=<?php echo $category->id ?>&url=<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
        </tr>
    <?php } ?>
</table>

<script type="text/javascript">
    jQuery( ".categoryDeleteButton" ).click( function( event ) {
        event.preventDefault();
        if( confirm( "Warning!  This will delete any past or present contests linked to this category.  Are you sure you want to delete this category?" ) ) {
            window.location="<?php echo esc_url( admin_url('admin-post.php') ); ?>?action=category_delete&id=" + jQuery( event.currentTarget ).data( "category" ) + "&url=<?php echo "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>";
        }
    } );
</script>
<?php } else { ?>
<div>
    There are no categories to display.  
</div>
<?php } ?>