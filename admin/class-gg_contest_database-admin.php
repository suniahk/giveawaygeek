<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gg_contest_database
 * @subpackage Gg_contest_database/admin
 * @author     Alex Watson <cleric.techie@gmail.com>
 */
class Gg_contest_database_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gg_contest_database_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gg_contest_database_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gg_contest_database-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( "flag-icon", plugin_dir_url( __FILE__ ) . 'css/flag-icon.css' );
		wp_enqueue_style( "fontawesome", plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gg_contest_database_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gg_contest_database_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gg_contest_database-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( "sortable", plugin_dir_url( __FILE__ ) . 'js/sortable.js' );

	}

	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function load_admin_page_contests() {
		require_once( plugin_dir_path( __FILE__ ) . 'partials/gg_contest_database-admin-contests.php' );
	}

	public function load_admin_page_categories() {
		require_once( plugin_dir_path( __FILE__ ) . 'partials/gg_contest_database-admin-categories.php' );
	}

	public function add_admin_page() {
		add_menu_page(
			'GiveawayGeek Database',
			'Contest Admin',
			'manage_options',
			$this->plugin_name,
			array( $this, 'load_admin_page_contests' ), // Calls function to require the partial
			"dashicons-smiley",
			6
		);

		add_submenu_page(
			$this->plugin_name,
			'Contest Categories',
			'Contest Categories',
			'manage_options',
			$this->plugin_name . "_categories",
			array( $this, 'load_admin_page_categories' ), // Calls function to require the partial
			"dashicons-list-view",
			6
		);
	}

	public function saveCategory() {
		global $wpdb;
		if( isset( $_POST[ "categoryName" ] ) )	{
			$newCategory = array(
				"category_name" => sanitize_text_field( $_POST[ "categoryName" ] )
			);

			if( $wpdb->insert( $wpdb->prefix . "gg_database_category", $newCategory ) ) {
				$url = sanitize_text_field( $_POST[ 'formUrl' ] );

				if( !isset( $_GET[ 'savedCategory' ] ) ) {
					if( strpos( $url, "?" ) > 0 ) {
						$url = $url . "&";
					} else {
						$url= $url . "?";
					}

					$url = $url . "savedCategory=true";
				}
				wp_redirect( $url );
			}
		}

		$url = sanitize_text_field( $_POST[ 'formUrl' ] );

		if( !isset( $_GET[ 'savedCategory' ] ) ) {
			if( strpos( $url, "?" ) > 0 ) {
				$url = $url . "&";
			} else {
				$url= $url . "?";
			}

			$url = $url . "savedCategory=false";
		}
		wp_redirect( $url );
	}

	public function makeFeatured() {
		global $wpdb;
		if( isset( $_GET[ "id" ] ) ) {
			$id = sanitize_text_field( $_GET[ "id" ] );
			$wpdb->update( $wpdb->prefix . "gg_database_contest", array( "is_featured" => true ), array( "id" => $id ) );
		}

		$url = sanitize_text_field( $_GET[ 'url' ] );
		wp_redirect( $url );
	}

	public function removeFeaturedStatus() {
		global $wpdb;
		if( isset( $_GET[ "id" ] ) ) {
			$id = sanitize_text_field( $_GET[ "id" ] );
			$wpdb->update( $wpdb->prefix . "gg_database_contest", array( "is_featured" => false ), array( "id" => $id ) );
		}

		$url = sanitize_text_field( $_GET[ 'url' ] );
		wp_redirect( $url );
	}

	public function setWeight() {
		global $wpdb;
		if( isset( $_GET[ "id" ] ) ) {
			$id = sanitize_text_field( $_GET[ "id" ] );
			$wpdb->update( $wpdb->prefix . "gg_database_contest", array( "weight" => $_GET[ "weight" ] ), array( "id" => $id ) );
		}

		$url = sanitize_text_field( $_GET[ 'url' ] );
		wp_redirect( $url );
	}

	public function deleteCategory() {
		global $wpdb;
		if( isset( $_GET[ "id" ] ) ) {
			$id = sanitize_text_field( $_GET[ "id" ] );
			if( $wpdb->delete( $wpdb->prefix . "gg_database_category", array( "id" => $id ) ) ) {
				if($wpdb->get_results( "DELETE FROM " . $wpdb->prefix . "gg_database_contest WHERE id NOT IN ( SELECT contest_id FROM " . $wpdb->prefix . "gg_contest_category )" ) ) {
					$url = sanitize_text_field( $_POST[ 'formUrl' ] );

					if( !isset( $_GET[ 'deletedCategory' ] ) ) {
						if( strpos( $url, "?" ) > 0 ) {
							$url = $url . "&";
						} else {
							$url= $url . "?";
						}

						$url = $url . "deletedCategory=true";
					}
					wp_redirect( $url );
				}
			}
		}

		$url = sanitize_text_field( $_GET[ 'url' ] );

		if( !isset( $_GET[ 'deletedCategory' ] ) ) {
			if( strpos( $url, "?" ) > 0 ) {
				$url = $url . "&";
			} else {
				$url= $url . "?";
			}

			$url = $url . "deletedCategory=false";
		}

		wp_redirect( $url );
	}

	public function deleteContest() {
		global $wpdb;
		if( isset( $_GET[ "id" ] ) ) {
			$id = sanitize_text_field( $_GET[ "id" ] );
			if( $wpdb->delete( $wpdb->prefix . "gg_database_contest", array( "id" => $id ) ) ) {
				$url = sanitize_text_field( $_POST[ 'formUrl' ] );

				if( !isset( $_GET[ 'deletedContest' ] ) ) {
					if( strpos( $url, "?" ) > 0 ) {
						$url = $url . "&";
					} else {
						$url= $url . "?";
					}

					$url = $url . "deletedContest=true";
				}
				wp_redirect( $url );
			}
		}

		$url = sanitize_text_field( $_GET[ 'url' ] );

		if( !isset( $_GET[ 'deletedContest' ] ) ) {
			if( strpos( $url, "?" ) > 0 ) {
				$url = $url . "&";
			} else {
				$url= $url . "?";
			}

			$url = $url . "deletedContest=false";
		}

		wp_redirect( $url );
	}
}
