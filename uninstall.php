<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       http://alexwatson.ca
 * @since      1.0.0
 *
 * @package    Gg_contest_database
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
global $wpdb;

	$sql[0]="DROP TABLES IF EXISTS " . $wpdb->prefix . "gg_contest_country;";
	$sql[1]="DROP TABLES IF EXISTS " . $wpdb->prefix . "gg_contest_category;";
	$sql[2]="DROP TABLES IF EXISTS " . $wpdb->prefix . "gg_database_contest;";
	$sql[3]="DROP TABLES IF EXISTS " . $wpdb->prefix . "gg_database_category;";
	$sql[4]="DROP TABLES IF EXISTS " . $wpdb->prefix . "gg_database_country;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	foreach( $sql as $query ) {
		dbDelta( $query );
	}
?>