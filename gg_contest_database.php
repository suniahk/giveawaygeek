<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://alexwatson.ca
 * @since             1.0.0
 * @package           Gg_contest_database
 *
 * @wordpress-plugin
 * Plugin Name:       GiveawayGeek Contest Database
 * Plugin URI:        http://thegiveawaygeek.com/
 * Description:       A plugin created to display contests for The Giveaway Geek
 * Version:           1.1.10a
 * Author:            Alex Watson
 * Author URI:        http://alexwatson.ca
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gg_contest_database
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gg_contest_database-activator.php
 */
function activate_gg_contest_database() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gg_contest_database-activator.php';
	Gg_contest_database_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gg_contest_database-deactivator.php
 */
function deactivate_gg_contest_database() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gg_contest_database-deactivator.php';
	Gg_contest_database_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gg_contest_database' );
register_deactivation_hook( __FILE__, 'deactivate_gg_contest_database' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gg_contest_database.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gg_contest_database() {

	$plugin = new Gg_contest_database();
	$plugin->run();

}
run_gg_contest_database();
