GiveawayGeek Wordpress Contest Database Plugin
========

Basic database with additional user features such as contest saving and sorting.

## Usage

* Upload and enable the plugin
* Create a page and add the shortcode [gg_contest_list featuredcount=2 contestsperpage=10] for retrieving a paginated list of contests.
  * _featuredcount_ (default 2) is the number maximum of featured contests to show per page.
  * _contestsperpage_ (default 10) is the total number of contests to show, including the featured count, per page.
* Create a page and add the shortcode [gg_contest_form] for showing the public form for adding contests.  This form uses ReCaptcha for security.
* Create a page and add the shortcode [gg_contest_latest days=7] for getting a listing of only the most recent contests.
  * _days_ (default 7) is the number of days to go back in time to find contests for.